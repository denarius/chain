# Denarius Chaindata Repository

This repo hodls Denarius (D) chaindata with Git LFS, since August of 2019 due to our chaindata becoming slightly over 2GB.

You can download the chaindata.zips by going to the "Raw" version here for future reference, watch or star this repo.

Original Denarius Repo: https://github.com/carsenk/denarius